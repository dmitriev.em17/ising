import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt

from ising.ising import Config

N = 10
beta = 0.8
J = -1

steps = 10

if __name__ == "__main__":
    conf = Config(
        size=N,
        beta=beta,
        J=J,
    )
    # E = []

    # for step in range(steps):
    #     print(step)
    #     conf.mcmove()
    #     E.append(conf.step())

    # plt.plot([i for i in range(len(E))], E)
    # plt.xlabel("iterations")
    # plt.ylabel("Energy")
    # plt.title("E(i)")
    # plt.show()

    # mass defect
    hole = [N // 2, N // 2]
    betas = np.linspace(0, 4, num=300)
    defect_mass = []

    for i in tqdm(range(len(betas))):
        masses = []

        C1 = Config(
            size=N,
            beta=betas[i],
            J=J,
        )
        C1.term(steps)
        C2 = Config(
            size=N,
            beta=betas[i],
            J=J,
        )
        C2.make_hole(hole[0], hole[1])
        C2.term(steps)

        for j in range(50):
            mass = 0

            C1.term(10)
            mass1 = C1.calc_energy()
            C2.term(10)
            mass2 = C2.calc_energy()

            masses.append(mass1 - mass2)

        defect_mass.append(np.mean(masses))

    plt.plot(betas, defect_mass)
    plt.xlabel("Бета")
    plt.ylabel("Масса дефекта")
    plt.show()
