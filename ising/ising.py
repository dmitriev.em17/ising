import numpy as np
from numpy.random import rand


class Config:
    def __init__(
        self,
        size: int,
        beta: float,
        J: float,
    ):
        self.s = size
        self.beta = beta
        self.J = J

        self.state = np.ones((size, size))
        self.energy = self.calc_energy()

    def mcmove(self):
        new_state = np.copy(self.state)

        for i in range(self.s):
            for j in range(self.s):
                s = new_state[i, j]
                nb = (
                    self.beta
                    * self.J
                    * (
                        new_state[(i + 1) % self.s, j]
                        + new_state[i, (j + 1) % self.s]
                        + new_state[(i - 1) % self.s, j]
                        + new_state[i, (j - 1) % self.s]
                    )
                )
                px = np.exp(nb) / (np.exp(nb) + np.exp(-nb))

                if rand() < px:
                    s *= -1

                new_state[i, j] = s

        self.state = new_state

    def calc_energy(self):
        """
        Energy of a given configuration
        """
        energy = 0

        for i in range(self.s):
            for j in range(self.s):
                S = self.state[i, j]
                nb = (
                    self.state[(i + 1) % self.s, j]
                    + self.state[i, (j + 1) % self.s]
                    + self.state[(i - 1) % self.s, j]
                    + self.state[i, (j - 1) % self.s]
                )
                energy += -nb * S
        return energy / 2.0

    def upd_energy(self):
        self.energy = self.calc_energy()

    def step(self) -> float:
        self.mcmove()
        self.upd_energy()
        return self.energy

    def term(self, steps):
        for _ in range(steps):
            self.step()

    def make_hole(self, i, j):
        self.state[i][j] = 0
